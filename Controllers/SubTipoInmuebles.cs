﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api_Ventas.Models;
using Api_Ventas.Models.Tables;

namespace Api_Ventas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubTipoInmuebles : ControllerBase
    {
        private readonly DbVentas _context;

        public SubTipoInmuebles(DbVentas context)
        {
            _context = context;
        }

        // GET: api/SubTipoInmuebles
        [HttpGet]
        public List<SubTipoInmueble> Get()
        {
            return  _context.SubTipoInmueble.ToList();
        }

        // GET: api/SubTipoInmuebles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubTipoInmueble>> GetSubTipoInmueble(int id)
        {
            var subTipoInmueble = await _context.SubTipoInmueble.FindAsync(id);

            if (subTipoInmueble == null)
            {
                return NotFound();
            }

            return subTipoInmueble;
        }

        // PUT: api/SubTipoInmuebles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubTipoInmueble(int id, SubTipoInmueble subTipoInmueble)
        {
            if (id != subTipoInmueble.idSubTipoInmueble)
            {
                return BadRequest();
            }

            _context.Entry(subTipoInmueble).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubTipoInmuebleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SubTipoInmuebles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<SubTipoInmueble>> PostSubTipoInmueble(SubTipoInmueble subTipoInmueble)
        {
            _context.SubTipoInmueble.Add(subTipoInmueble);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubTipoInmueble", new { id = subTipoInmueble.idSubTipoInmueble }, subTipoInmueble);
        }

        // DELETE: api/SubTipoInmuebles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SubTipoInmueble>> DeleteSubTipoInmueble(int id)
        {
            var subTipoInmueble = await _context.SubTipoInmueble.FindAsync(id);
            if (subTipoInmueble == null)
            {
                return NotFound();
            }

            _context.SubTipoInmueble.Remove(subTipoInmueble);
            await _context.SaveChangesAsync();

            return subTipoInmueble;
        }

        private bool SubTipoInmuebleExists(int id)
        {
            return _context.SubTipoInmueble.Any(e => e.idSubTipoInmueble == id);
        }
    }
}
