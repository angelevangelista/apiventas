﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api_Ventas.Models;
using Api_Ventas.Models.Tables;

namespace Api_Ventas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TagsInmuebles : ControllerBase
    {
        private readonly DbVentas _context;

        public TagsInmuebles(DbVentas context)
        {
            _context = context;
        }

        [HttpGet]
        public List<TagsInmueble> Get()
        {
            return  _context.TagsInmueble.ToList();
        }

        // GET: api/TagsInmuebles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TagsInmueble>> GetTagsInmueble(int id)
        {
            var tagsInmueble = await _context.TagsInmueble.FindAsync(id);

            if (tagsInmueble == null)
            {
                return NotFound();
            }

            return tagsInmueble;
        }

        // PUT: api/TagsInmuebles/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTagsInmueble(int id, TagsInmueble tagsInmueble)
        {
            if (id != tagsInmueble.idTags)
            {
                return BadRequest();
            }

            _context.Entry(tagsInmueble).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TagsInmuebleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/TagsInmuebles
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<TagsInmueble>> PostTagsInmueble(TagsInmueble tagsInmueble)
        {
            _context.TagsInmueble.Add(tagsInmueble);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTagsInmueble", new { id = tagsInmueble.idTags }, tagsInmueble);
        }

        // DELETE: api/TagsInmuebles/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<TagsInmueble>> DeleteTagsInmueble(int id)
        {
            var tagsInmueble = await _context.TagsInmueble.FindAsync(id);
            if (tagsInmueble == null)
            {
                return NotFound();
            }

            _context.TagsInmueble.Remove(tagsInmueble);
            await _context.SaveChangesAsync();

            return tagsInmueble;
        }

        private bool TagsInmuebleExists(int id)
        {
            return _context.TagsInmueble.Any(e => e.idTags == id);
        }
    }
}
