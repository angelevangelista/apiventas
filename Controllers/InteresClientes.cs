﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Api_Ventas.Models;
using Api_Ventas.Models.Tables;

namespace Api_Ventas.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InteresClientes : ControllerBase
    {
        private readonly DbVentas _context;

        public InteresClientes(DbVentas context)
        {
            _context = context;
        }

        // GET: api/InteresClientes
        [HttpGet]
        public List<InteresCliente> Get()
        {
            return  _context.InteresCliente.ToList();
        }

        // GET: api/InteresClientes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InteresCliente>> GetInteresCliente(int id)
        {
            var interesCliente = await _context.InteresCliente.FindAsync(id);

            if (interesCliente == null)
            {
                return NotFound();
            }

            return interesCliente;
        }

        // PUT: api/InteresClientes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInteresCliente(int id, InteresCliente interesCliente)
        {
            if (id != interesCliente.idInteresCliente)
            {
                return BadRequest();
            }

            _context.Entry(interesCliente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InteresClienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InteresClientes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<InteresCliente>> PostInteresCliente(InteresCliente interesCliente)
        {
            _context.InteresCliente.Add(interesCliente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInteresCliente", new { id = interesCliente.idInteresCliente }, interesCliente);
        }

        // DELETE: api/InteresClientes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<InteresCliente>> DeleteInteresCliente(int id)
        {
            var interesCliente = await _context.InteresCliente.FindAsync(id);
            if (interesCliente == null)
            {
                return NotFound();
            }

            _context.InteresCliente.Remove(interesCliente);
            await _context.SaveChangesAsync();

            return interesCliente;
        }

        private bool InteresClienteExists(int id)
        {
            return _context.InteresCliente.Any(e => e.idInteresCliente == id);
        }
    }
}
