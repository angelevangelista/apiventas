﻿using System.Collections.Generic;
using System.Linq;
using Api_Ventas.Models;
using Api_Ventas.Models.Tables;
using Microsoft.AspNetCore.Mvc;


namespace Api_Ventas.Controllers
{
    [Route("api/[controller]")]
    public class Ubicaciones : Controller
    {
        private DbVentas _context;

        public Ubicaciones(DbVentas context)
        {
            _context = context;
        }

        [HttpGet]
        public List<Ubicacion> Get()
        {
            return _context.Ubicacion.ToList();
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
