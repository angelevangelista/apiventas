﻿using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class Cliente
    {
        [Key]
        public int idCliente { get; set; }
        public bool Estado { get; set; }
        public string Celular { get; set; }
        public string Telefono { get; set; }
        public string RangoPrecio { get; set; }
        public int TipoInmuebleid { get; set; }
        public int SubTipoInmuebleid { get; set; }
        public int InteresClienteid { get; set; }
        public int Ubicacionid { get; set; }


        #region Propiedades externas

        public TipoInmueble TipoInmueble { get; set; }
        public SubTipoInmueble SubTipoInmueble  { get; set; }
        public InteresCliente InteresCliente { get; set; }
        public Ubicacion Ubicacione { get; set; }



        #endregion
    }
}
