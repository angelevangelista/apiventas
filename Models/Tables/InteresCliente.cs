﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class InteresCliente
    {
        [Key]
        public int idInteresCliente { get; set; }
        public string InteresClienteNombre { get; set; }

    }
}
