﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class SubTipoInmueble
    {
        [Key]
        public int idSubTipoInmueble { get; set; }
        public string SubTipoInmuebleNombre { get; set; }

    }
}
