﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class Ubicacion
    {
        [Key]
        public int idUbicacion { get; set; }
        public string UbicacionesNombre { get; set; }

    }

}
