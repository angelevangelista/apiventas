﻿using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class TagsInmuebleCliente
    {
        [Key]
        public int idTagClientes { get; set; }
        public int idCliente { get; set; }
        public int idTags { get; set; }

    }
}
