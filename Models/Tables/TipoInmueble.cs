﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Api_Ventas.Models.Tables
{
    public class TipoInmueble
    {
        [Key]
        public int idTipoInmueble { get; set; }
        public string TipoInmuebleNombre { get; set; }

    }
}
