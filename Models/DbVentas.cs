﻿using Api_Ventas.Models.Tables;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Api_Ventas.Models
{
    public class DbVentas: DbContext
    {
        protected readonly IConfiguration Configuration;

        public DbVentas(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // connect to Sql-Server DB
            optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DbContext"));
        }

        public virtual DbSet<Cliente> Cliente{ get; set; }
        public virtual DbSet<InteresCliente> InteresCliente{ get; set; }
        public virtual DbSet<SubTipoInmueble> SubTipoInmueble { get; set; }
        public virtual DbSet<TagsInmueble> TagsInmueble { get; set; }
        public virtual DbSet<TagsInmuebleCliente> TagsInmuebleCliente { get; set; }
        public virtual DbSet<TipoInmueble> TipoInmueble { get; set; }
        public virtual DbSet<Ubicacion> Ubicacion { get; set; }
    }
}
