create database VentasClientes
go

use VentasClientes

/*Se refiere a las caracteristicas que tiene un apartamento */
create table TagsInmuebles(
idTags int identity primary key,
TagsNombre varchar(40)
)
go


/*Se refiere a si es casa o apartamento*/
create table TipoInmueble(
idTipoInmueble int identity primary key,  
TipoInmuebleNombre varchar (40)
)


/*Se refiere a tipo de inmueble que el cliente espera, sea tama�o, tipo A, B o C*/
create table SubTipoInmueble(
idSubTipoInmueble int identity primary key,
SubTipoInmuebleNombre varchar(90)
)
go

/*Zona este, oeste, Pablo Mella, etc*/
create table Ubicaciones(
idUbicaciones int identity primary key,
UbicacionesNombre varchar(20)
)
go


/*Se refiere a que el cliente espera hacer con ese inmueble, alquilar vender etc*/
create table InteresCliente(
idInteresCliente int identity primary key,
InteresClienteNombre varchar(30)
)



create table Clientes(
idCliente int identity primary key,
Estado bit,
Celular varchar(10),
Telefono varchar(10),
RangoPrecio varchar(20),
idTipoInmueble int FOREIGN KEY REFERENCES TipoInmueble(idTipoInmueble),
idSubTipoInmueble int FOREIGN KEY REFERENCES SubTipoInmueble(idSubTipoInmueble),
idInteresCliente int FOREIGN KEY REFERENCES InteresCliente(idInteresCliente),
idUbicaciones int FOREIGN KEY REFERENCES Ubicaciones(idUbicaciones)
)

/*Se refiere a las caracteristicas de esa casa u apartamento para el cliente*/
create table TagsInmueblesClientes(
idTagClientes int identity primary key,
idCliente int FOREIGN KEY REFERENCES Clientes(idCliente),
idTags int FOREIGN KEY REFERENCES TagsInmuebles(idTags),
)
go



